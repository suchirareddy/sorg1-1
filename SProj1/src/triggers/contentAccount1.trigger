trigger contentAccount1 on ContentVersion (before insert) {
	List<ContentVersion> cv=trigger.new;
    Id id1 = UserInfo.getProfileId();
	Profile prof = [Select Name from Profile where Id = :id1 limit 1];
    if(prof.name=='System Administrator'){
    for(ContentVersion c:cv){
        
        c.addError('No Permission');
    }
    }
}