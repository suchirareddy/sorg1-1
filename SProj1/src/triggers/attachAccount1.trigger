trigger attachAccount1 on Attachment (before insert) {
	list<Attachment> att=trigger.New;
    
    Id id1 = UserInfo.getProfileId();
	Profile prof = [Select Name from Profile where Id = :id1 limit 1];
    if(prof.name=='System Administrator'){
    	for(attachment a:att){
        	a.addError('You donot have permission to add an attachment');
    	}
    }
}